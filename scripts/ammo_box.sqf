// -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-
// Crate loading script for "Transport & Ambush"
// Written by Kamaradski & Jester 2014
// -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-

if (!isServer) exitWith {};

kamAllowedRocketBLUECrates = [box1];
kamAllowedRocketREDCrates = [box2];
kamAllowedMortarBLUECrates = [box3];
kamAllowedMortarREDCrates = [];


while {true} do {

	kamActiveRocketBLUECrates = [];
	kamActiveRocketREDCrates = [];
	kamActiveMortarBLUECrates = [];
	kamActiveMortarREDCrates = [];


// Check if alive ROCKET-BLUE-CRATES
	{
		if (alive _x) then {
			kamActiveRocketBLUECrates = kamActiveRocketBLUECrates + [_x];
		};
		sleep 0.1;
	} foreach kamAllowedRocketBLUECrates;

// Populate ROCKET-BLUE-CRATES
	{
		clearWeaponCargoGlobal _x;
		clearMagazineCargoGlobal _x;
		clearBackpackCargoGlobal _x;
		clearItemCargoGlobal  _x;
		_x addMagazineCargoGlobal ["NLAW_F",2];
		_x addMagazineCargoGlobal ["Titan_AT",2];
		_x addMagazineCargoGlobal ["Titan_AP",2];
		_x addMagazineCargoGlobal ["Titan_AA",2];
		sleep 0.1;
	} foreach kamAllowedRocketBLUECrates;


// Check if alive ROCKET-RED-CRATES
	{
		if (alive _x) then {
			kamActiveRocketREDCrates = kamActiveRocketREDCrates + [_x];
		};
		sleep 0.1;
	} foreach kamAllowedRocketREDCrates;

// Populate ROCKET-RED-CRATES
	{
		clearWeaponCargoGlobal _x;
		clearMagazineCargoGlobal _x;
		clearBackpackCargoGlobal _x;
		clearItemCargoGlobal  _x;
		_x addMagazineCargoGlobal ["RPG32_F",2];
		_x addMagazineCargoGlobal ["RPG32_HE_F",2];
		_x addMagazineCargoGlobal ["Titan_AT",2];
		_x addMagazineCargoGlobal ["Titan_AP",2];
		_x addMagazineCargoGlobal ["Titan_AA",2];
		sleep 0.1;
	} foreach kamAllowedRocketREDCrates;



// Check if alive MORTAR-BLUE-CRATES
	{
		if (alive _x) then {
			kamActiveMortarBLUECrates = kamActiveMortarBLUECrates + [_x];
		};
		sleep 0.1;
	} foreach kamAllowedMortarBLUECrates;

// Populate MORTAR-BLUE-CRATES
	{
		clearWeaponCargoGlobal _x;
		clearMagazineCargoGlobal _x;
		clearBackpackCargoGlobal _x;
		clearItemCargoGlobal  _x;
		_x addBackpackCargoGlobal ["B_UAV_01_backpack_F",1];
		_x addBackpackCargoGlobal ["B_Mortar_01_support_F",2];
		sleep 0.1;
	} foreach kamAllowedMortarBLUECrates;




// Check if alive MORTAR-RED-CRATES
	{
		if (alive _x) then {
			kamActiveMortarREDCrates = kamActiveMortarREDCrates + [_x];
		};
		sleep 0.1;
	} foreach kamAllowedMortarREDCrates;

// Populate MORTAR-RED-CRATES
	{
		clearWeaponCargoGlobal _x;
		clearMagazineCargoGlobal _x;
		clearBackpackCargoGlobal _x;
		clearItemCargoGlobal  _x;
		_x addBackpackCargoGlobal ["O_UAV_01_backpack_F",1];
		_x addBackpackCargoGlobal ["O_Mortar_01_support_F",2];
		sleep 0.1;
	} foreach kamAllowedMortarREDCrates;


sleep 120;

}