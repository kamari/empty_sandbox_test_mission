// -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-
// VAS config scriptfor "Transport & Ambush"
// Written by Tonic 2013
//
// Edited by Kamaradski & Jester 2014
// Classname list and side affinity by StuffedSheep 2014
// -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-

//Allow player to respawn with his loadout? If true unit will respawn with all ammo from initial save! Set to false to disable this and rely on other scripts!
vas_onRespawn = true;
//Preload Weapon Config?
vas_preload = true;
//If limiting weapons its probably best to set this to true so people aren't loading custom loadouts with restricted gear.
vas_disableLoadSave = true;
//Amount of save/load slots
vas_customslots = 9; //9 is actually 10 slots, starts from 0 to whatever you set, so always remember when setting a number to minus by 1, i.e 12 will be 11.
//Disable 'VAS hasn't finished loading' Check !!! ONLY RECOMMENDED FOR THOSE THAT USE ACRE AND OTHER LARGE ADDONS !!!
vas_disableSafetyCheck = false;



switch(playerSide) do
	{
		//Blufor
		case west:
		{
			vas_weapons =
			[
				"hgun_P07_F",
				"hgun_Pistol_heavy_01_F",
				"SMG_01_F",
				"arifle_MX_F",
				"arifle_MXC_F",
				"arifle_MXM_F",
				"arifle_MX_GL_F",
				"arifle_MX_SW_F",
				"arifle_MX_Black_F",
				"arifle_MXC_Black_F",
				"arifle_MXM_Black_F",
				"arifle_MX_GL_Black_F",
				"arifle_MX_SW_Black_F",
				"srifle_LRR_F",
				"launch_NLAW_F",
				"launch_B_Titan_F",
				"launch_B_Titan_short_F"
			];

			vas_magazines =
			[];

			vas_items =
			[
				"U_B_CombatUniform_mcam",
				"U_B_CombatUniform_mcam_tshirt",
				"U_B_CombatUniform_mcam_vest",
				"U_B_CombatUniform_mcam_worn",
				"U_B_CombatUniform_sgg",
				"U_B_CombatUniform_sgg_tshirt",
				"U_B_CombatUniform_sgg_vest",
				"U_B_CombatUniform_wdl",
				"U_B_CombatUniform_wdl_tshirt",
				"U_B_CombatUniform_wdl_vest",
				"U_B_SpecopsUniform_sgg",
				"U_B_GhillieSuit",
				"U_B_HeliPilotCoveralls",
				"U_B_PilotCoveralls",
				"U_B_Wetsuit",
				"U_B_CTRG_1",
				"U_B_CTRG_2",
				"U_B_CTRG_3",
				"U_B_survival_uniform",
				"V_PlateCarrier1_blk",
				"V_PlateCarrier1_rgr",
				"V_PlateCarrier2_rgr",
				"V_PlateCarrier3_rgr",
				"V_PlateCarrierGL_rgr",
				"V_PlateCarrierSpec_rgr",
				"V_PlateCarrierL_CTRG",
				"V_PlateCarrierH_CTRG",
				"V_PlateCarrier_Kerry",
				"V_Chestrig_khk",
				"V_Chestrig_rgr",
				"V_Chestrig_oli",
				"V_Chestrig_blk",
				"V_RebreatherB",
				"H_HelmetB",
				"H_HelmetB_camo",
				"H_HelmetB_paint",
				"H_HelmetB_grass",
				"H_HelmetB_snakeskin",
				"H_HelmetB_desert",
				"H_HelmetB_black",
				"H_HelmetB_sand",
				"H_HelmetB_light",
				"H_HelmetB_light_desert",
				"H_HelmetB_light_black",
				"H_HelmetB_light_sand",
				"H_HelmetB_light_grass",
				"H_HelmetB_light_snakeskin",
				"H_HelmetSpecB",
				"H_HelmetSpecB_paint1",
				"H_HelmetSpecB_paint2",
				"H_HelmetSpecB_blk",
				"H_HelmetB_plain_mcamo",
				"H_HelmetB_plain_blk",
				"H_HelmetCrew_B",
				"H_CrewHelmetHeli_B",
				"H_PilotHelmetHeli_B",
				"H_PilotHelmetFighter_B",
				"H_Bandanna_mcamo",
				"H_Booniehat_mcamo",
				"H_MilCap_mcamo",
				"H_Beret_grn_SF",
				"H_Beret_brn_SF",
				"H_Beret_02",
				"H_Cap_brn_SPECOPS",
				"H_Cap_tan_specops_US",
				"H_Cap_khaki_specops_UK",
				"B_UavTerminal",
				"ItemWatch",
				"ItemCompass",
				"ItemGPS",
				"ItemRadio",
				"ItemMap",
				"NVGoggles",
				"NVGoggles_OPFOR",
				"NVGoggles_INDEP",
				"FirstAidKit",
				"Medikit",
				"ToolKit",
				"Binocular",
				"Rangefinder",
				"MineDetector"
			];

			vas_backpacks =
			[
				"B_HMG_01_weapon_F",
				"B_GMG_01_weapon_F",
				"B_HMG_01_A_weapon_F",
				"B_GMG_01_A_weapon_F",
				"B_HMG_01_high_weapon_F",
				"B_GMG_01_high_weapon_F",
				"B_HMG_01_A_high_weapon_F",
				"B_GMG_01_A_high_weapon_F",
				"B_AT_01_weapon_F",
				"B_AA_01_weapon_F",
				"B_Mortar_01_weapon_F",
				"B_AssaultPack_rgr",
				"B_BergenC_grn",
				"B_Carryall_khk",
				"B_FieldPack_khk",
				"B_Kitbag_rgr",
				"B_OutdoorPack_blu",
				"B_TacticalPack_rgr",
				"B_Parachute"
			];

			vas_glasses = [];
		};


		//Opfor
		case east:
		{
			vas_weapons =
			[
				"hgun_Rook40_F",
				"hgun_Pistol_heavy_02_F",
				"SMG_02_F",
				"arifle_Katiba_F",
				"arifle_Katiba_C_F",
				"arifle_Katiba_GL_F",
				"LMG_Zafir_F",
				"srifle_DMR_01_F",
				"srifle_GM6_F",
				"launch_RPG32_F",
				"launch_O_Titan_F",
				"launch_O_Titan_short_F"
			];

			vas_magazines =
			[];

			vas_items =
			[
				"U_O_CombatUniform_ocamo",
				"U_O_CombatUniform_oucamo",
				"U_O_SpecopsUniform_blk",
				"U_O_SpecopsUniform_ocamo",
				"U_O_OfficerUniform_ocamo",
				"U_O_GhillieSuit",
				"U_O_PilotCoveralls",
				"U_O_Wetsuit",
				"V_HarnessO_brn",
				"V_HarnessOGL_brn",
				"V_HarnessOSpec_brn",
				"V_HarnessO_gry",
				"V_HarnessOGL_gry",
				"V_HarnessOSpec_gry",
				"V_RebreatherIR",
				"H_HelmetO_ocamo",
				"H_HelmetLeaderO_ocamo",
				"H_HelmetSpecO_ocamo",
				"H_HelmetO_oucamo",
				"H_HelmetLeaderO_oucamo",
				"H_HelmetSpecO_blk",
				"H_HelmetCrew_O",
				"H_CrewHelmetHeli_O",
				"H_PilotHelmetHeli_O",
				"H_PilotHelmetFighter_O",
				"H_Booniehat_khk",
				"H_MilCap_ocamo",
				"H_MilCap_oucamo",
				"H_Beret_ocamo",
				"O_UavTerminal",
				"ItemWatch",
				"ItemCompass",
				"ItemGPS",
				"ItemRadio",
				"ItemMap",
				"NVGoggles",
				"NVGoggles_OPFOR",
				"NVGoggles_INDEP",
				"FirstAidKit",
				"Medikit",
				"ToolKit",
				"Binocular",
				"Rangefinder",
				"MineDetector"
			];

			vas_backpacks =
			[
				"O_HMG_01_weapon_F",
				"O_GMG_01_weapon_F",
				"O_HMG_01_high_weapon_F",
				"O_GMG_01_high_weapon_F",
				"O_Mortar_01_weapon_F",
				"B_AssaultPack_rgr",
				"B_BergenC_grn",
				"B_Carryall_khk",
				"B_FieldPack_khk",
				"B_Kitbag_rgr",
				"B_OutdoorPack_blu",
				"B_TacticalPack_rgr",
				"B_Parachute"
			];

			vas_glasses = [];
		};
	};



// RESTRICTED FOR EVERYONE !!!

vas_r_weapons = [
"hgun_ACPC2_F",
"hgun_PDW2000_F",
"arifle_Mk20_F",
"arifle_Mk20C_F",
"arifle_Mk20_GL_F",
"arifle_Mk20_plain_F",
"arifle_Mk20C_plain_F",
"arifle_Mk20_GL_plain_F",
"LMG_Mk200_F",
"srifle_EBR_F",
"srifle_GM6_F",
"launch_I_Titan_F",
"launch_I_Titan_short_F",
"arifle_TRG20_F",
"arifle_TRG21_F",
"arifle_TRG21_GL_F",
"arifle_SDAR_F",
"launch_Titan_F",
"launch_Titan_short_F"
];

vas_r_backpacks = [
"B_HMG_01_support_F",
"B_HMG_01_support_high_F",
"B_Mortar_01_support_F",
"I_HMG_01_weapon_F",
"I_GMG_01_weapon_F",
"I_HMG_01_high_weapon_F",
"I_GMG_01_high_weapon_F",
"I_HMG_01_support_F",
"I_HMG_01_support_high_F",
"I_Mortar_01_weapon_F",
"I_Mortar_01_support_F",
"O_HMG_01_support_F",
"O_GMG_01_support_high_F",
"O_Mortar_01_support_F",
"B_UAV_01_backpack_F",
"O_UAV_01_backpack_F",
"I_UAV_01_backpack_F"
];

vas_r_magazines = [
"NLAW_F",
"RPG32_F",
"RPG32_HE_F",
"Titan_AA",
"Titan_AT",
"Titan_AP"
];

vas_r_items = [
"optic_NVS",
"optic_Nightstalker",
"optic_tws",
"optic_tws_mg",
"U_I_CombatUniform",
"U_I_CombatUniform_shortsleeve",
"U_I_CombatUniform_tshirt",
"U_I_OfficerUniform",
"U_I_GhillieSuit",
"U_I_HeliPilotCoveralls",
"U_I_pilotCoveralls",
"U_I_Wetsuit",
"U_IG_Guerilla1_1",
"U_IG_Guerilla2_1",
"U_IG_Guerilla2_2",
"U_IG_Guerilla2_3",
"U_IG_Guerilla3_1",
"U_IG_Guerilla3_2",
"U_IG_leader",
"U_BG_Guerilla1_1",
"U_BG_Guerilla2_1",
"U_BG_Guerilla2_2",
"U_BG_Guerilla2_3",
"U_BG_Guerilla3_1",
"U_BG_Guerilla3_2",
"U_BG_leader",
"U_OG_Guerilla1_1",
"U_OG_Guerilla2_1",
"U_OG_Guerilla2_2",
"U_OG_Guerilla2_3",
"U_OG_Guerilla3_1",
"U_OG_Guerilla3_2",
"U_OG_leader",
"U_C_Poloshirt_blue",
"U_C_Poloshirt_burgundy",
"U_C_Poloshirt_stripped",
"U_C_Poloshirt_tricolour",
"U_C_Poloshirt_salmon",
"U_C_Poloshirt_redwhite",
"U_C_Commoner1_1",
"U_C_Commoner1_2",
"U_C_Commoner1_3",
"U_Rangemaster",
"U_Competitor",
"U_C_Poor_1",
"U_C_Poor_2",
"U_C_Scavenger_1",
"U_C_Scavenger_2",
"U_C_Farmer",
"U_C_Fisherman",
"U_C_WorkerOveralls",
"U_C_FishermanOveralls",
"U_C_WorkerCoveralls",
"U_C_HunterBody_grn",
"U_C_HunterBody_brn",
"U_C_Commoner2_1",
"U_C_Commoner2_2",
"U_C_Commoner2_3",
"U_C_PriestBody",
"U_C_Poor_shorts_1",
"U_C_Poor_shorts_2",
"U_C_Commoner_shorts",
"U_C_ShirtSurfer_shorts",
"U_C_TeeSurfer_shorts_1",
"U_C_TeeSurfer_shorts_2",
"U_NikosBody",
"U_MillerBody",
"U_KerryBody",
"U_OrestesBody",
"U_AttisBody",
"U_AntigonaBody",
"U_IG_Menelaos",
"U_C_Novak",
"U_OI_Scientist",
"V_PlateCarrierIA1_dgtl",
"V_PlateCarrierIA2_dgtl",
"V_PlateCarrierIAGL_dgtl",
"V_RebreatherIA",
"V_TacVest_khk",
"V_TacVest_brn",
"V_TacVest_oli",
"V_TacVest_blk",
"V_TacVest_camo",
"V_TacVest_blk_POLICE",
"V_TacVestIR_blk",
"V_TacVestCamo_khk",
"V_I_G_resistanceLeader_F",
"V_Rangemaster_belt",
"V_BandollierB_khk",
"V_BandollierB_cbr",
"V_BandollierB_rgr",
"V_BandollierB_blk",
"V_BandollierB_oli",
"H_HelmetIA",
"H_HelmetIA_net",
"H_HelmetIA_camo",
"H_HelmetCrew_I",
"H_CrewHelmetHeli_I",
"H_PilotHelmetHeli_I",
"H_PilotHelmetFighter_I",
"H_Booniehat_dgtl",
"H_Booniehat_indp",
"H_MilCap_dgtl",
"H_Cap_grn",
"H_Cap_red",
"H_Cap_blu",
"H_Cap_oli",
"H_Cap_tan",
"H_Cap_blk",
"H_Cap_grn_BI",
"H_Cap_blk_Raven",
"H_Cap_blk_ION",
"H_Cap_blk_CMMG",
"H_MilCap_rucamo",
"H_MilCap_gry",
"H_MilCap_blue",
"H_Cap_headphones",
"H_Booniehat_grn",
"H_Booniehat_tan",
"H_Booniehat_dirty",
"H_StrawHat",
"H_StrawHat_dark",
"H_Hat_blue",
"H_Hat_brown",
"H_Hat_camo",
"H_Hat_grey",
"H_Hat_checker",
"H_Hat_tan",
"H_Bandanna_surfer",
"H_Bandanna_khk",
"H_Bandanna_cbr",
"H_Bandanna_sgg",
"H_Bandanna_gry",
"H_Bandanna_camo",
"H_TurbanO_blk",
"H_Shemag_khk",
"H_Shemag_tan",
"H_Shemag_olive",
"H_ShemagOpen_khk",
"H_ShemagOpen_tan",
"H_Beret_blk",
"H_Beret_blk_POLICE",
"H_Beret_red",
"H_Beret_grn",
"H_Watchcap_blk",
"H_Watchcap_khk",
"H_Watchcap_camo",
"H_Watchcap_sgg",
"H_BandMask_blk",
"H_BandMask_khk",
"H_BandMask_reaper",
"H_BandMask_demon",
"H_Shemag_olive_hs",
"H_Cap_oli_hs",
"H_Bandanna_khk_hs",
"I_UavTerminal",
"H_Booniehat_khk_hs",
"Laserdesignator"
];

vas_r_glasses = [];
