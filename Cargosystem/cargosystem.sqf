// -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-
// Cargo system script for "Transport & Ambush"
// Written by Kamaradski 2014
// -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-



// -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-
// Parsing Settings
// -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-
execVM "Cargosystem\settings.sqf";
sleep 0.1;

// -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-
// Pre-setting variables:
// -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-

kARRcargoAddactionActive = [];											// Indicate if add action is active for this object
kARRselected =[];														// LOCAL-PLAYER: array of selected cargo
kINTloadmenuactive = 0;													// Indicator for the addaction menu item
kARRload = [];															// array of loaded items in truck
kARRholdcargo = [];														// Array containing cargo loaded in truck. Variable local to vehicle, networked
kARRloadedcargo = [];													// hold names of cargo currently loaded
kINTunloadmenuactive = 0;												// Switch for unloading menu

{
_x setvariable ["kARRholdcargo", [], true];								//pre-setting the 'cargo-loaded-in-truck' array for each truck
} forEach kARRvehicles;

// -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-
// Presetting functions:
// -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-

kFUNC_playerdead = {													// Things to do when player dies
	if ( !alive player ) then {											// if player dead:
		kARRselected = [];												// Empty selection
	};
};	

// -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-
// countdown:
// -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-

if (!isServer) then {													// CLIENT code
	if (isNil "kINTmissionstart") then									// if countdown is not set:
	{
		kINTmissionstart = 400; 										// Set countdown to 400 to prevent early start
	}; 
	"kINTmissionstart" addPublicVariableEventHandler {					// Create publiceventhandle for the countdown
		hint format ["mission start in: %1",kINTmissionstart];			// display countdown when eventhander is tripped
		};
	while {kINTmissionstart!=0} do {									// while countdown is not 0:
		sleep 1;														// sleep 1 second
	};
};

hint "Cargo system loaded";												// indicate onscreen that system is activated
sleep floor random 5;													// wait random between 0 & 5 seconds to prevent start-up lag

kINTcargoSystemActive = 1;


// -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-
// Booting scripts:
// -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-
execVM "Cargosystem\cargoselect.sqf";
execVM "Cargosystem\cargoload.sqf";
execVM "Cargosystem\cargounload.sqf";
execVM "Cargosystem\cargodamage.sqf";

// -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-
// Engine loop:
// -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-

while { kINTcargoSystemActive == 1 } do {
	call kFUNC_playerdead;
};



