// -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-
// Cargo loading script for "Transport & Ambush"
// Written by Kamaradski 2014
// -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-

kARRload = [];						// array of loaded items in truck

kFUNC_lockcargo = {																								// Function to lock all cargo positions on truck 
	kARRcrew = crew kOBJneartruck;																				// Check who is in the truck
	if (player in kARRcrew) then { 																				// if player in crew:
		if (player != driver kOBJneartruck) then {																// if player is not the driver:
			player action ["eject", kOBJneartruck]; 															// eject player from vehicle
		};
	};
	kOBJneartruck lockCargo [1, true];																			// lock cargo position
	kOBJneartruck lockCargo [2, true];
	kOBJneartruck lockCargo [3, true];
	kOBJneartruck lockCargo [4, true];
	kOBJneartruck lockCargo [5, true];
	kOBJneartruck lockCargo [6, true];
	kOBJneartruck lockCargo [7, true];
	kOBJneartruck lockCargo [8, true];
	kOBJneartruck lockCargo [9, true];
	kOBJneartruck lockCargo [10, true];
	kOBJneartruck lockCargo [11, true];
	kOBJneartruck lockCargo [12, true];
	kOBJneartruck lockCargo [13, true];
	kOBJneartruck lockCargo [14, true];
	kOBJneartruck lockCargo [15, true];
	kOBJneartruck lockCargo [16, true];
};																												// EOF

while {kINTcargoSystemActive == 1} do {
	if (count kARRselected != 0) then {																			// if some cargo is selected:
		if (kINTloadmenuactive == 0 ) then {																	// if no loading addaction is active yet
			_kARRnearobj = nearestObjects [player, ["Truck_01_base_F","Truck_02_base_F"], 10];					// list nearest trucks in array (OBJECTS)
			if ( count _kARRnearobj > 0 ) then {																// if _kARRnearobj is not empty:
				kOBJneartruck = _kARRnearobj select 0;															// Select the first nearest object
				kSTRneartruckname = vehicleVarName kOBJneartruck;												// Get name of nearest object (STRING)
				if ( kOBJneartruck in kARRvehicles ) then {														// check if one of the trucks is nearest to player
					kINTloadmenuactive = 1;																		// Set truck loading menu active
					kHANDloadaction = player addAction ["Load cargo in truck...", {								// add an addaction on the player to load cargo in nearest truck
						{
							kARRload = kOBJneartruck getVariable "kARRholdcargo";								// count the loaded content as this will change each loop
							call kFUNC_lockcargo;																// Lock cargo positions
							if ( count kARRload >= 2) then {													// if already 2 cargo's are in truck:
								hint "Sorry this truck is already full";										// Error message
							};
							if ( count kARRload == 1) then {													// If 1 cargo is already loaded:
								kSTRtrucktype = typeOf kOBJneartruck;											// Check type of nearest truck
								if ( kSTRtrucktype == "B_Truck_01_transport_F" ) then {							// If the nearest truck is HEMMT:
									kARRnewload = kARRload + [_x];
									_x attachTo [kOBJneartruck,[0.05,-3,0.25]];									// load the 2nd cargo into slot 2
									kOBJneartruck setvariable ["kARRholdcargo", kARRnewload, true];				// Set loaded cargo to 2
									kARRselected = kARRselected - [_x];											// remove the cargo from the active selection
									kARRloadedcargo = kARRloadedcargo + [_x];									// add cargo to loaded list
									publicVariable "kARRloadedcargo";											// Transmit new loaded list
								};
								kSTRtrucktype = typeOf kOBJneartruck;											// Check type of nearest truck
								if ( kSTRtrucktype == "O_Truck_02_transport_F" ) then {							// If the nearest truck is KAMAZ:
									kARRnewload = kARRload + [_x];
									_x attachTo [kOBJneartruck,[0.07,-2.3,0.1]];								// load the 2nd cargo into slot 2
									kOBJneartruck setvariable ["kARRholdcargo", kARRnewload, true];				// Set loaded cargo to 2
									kARRselected = kARRselected - [_x];											// remove the cargo from the active selection
									kARRloadedcargo = kARRloadedcargo + [_x];									// add cargo to loaded list
									publicVariable "kARRloadedcargo";											// Transmit new loaded list
								};
							};
							if (count kARRload == 0) then {														// If 0 cargo is loaded:
								kSTRtrucktype = typeOf kOBJneartruck;											// Check type of nearest truck
								if ( kSTRtrucktype == "B_Truck_01_transport_F" ) then {							// If the nearest truck is HEMMT:
									kARRnewload = kARRload + [_x];
									_x attachTo [kOBJneartruck,[0.05,-1,0.25]];									// load the 1st cargo into slot 1
									kOBJneartruck setvariable ["kARRholdcargo", kARRnewload, true];				// Set loaded cargo to 1
									kARRselected = kARRselected - [_x];											// remove the cargo from the active selection
									kARRloadedcargo = kARRloadedcargo + [_x];									// add cargo to loaded list
									publicVariable "kARRloadedcargo";											// Transmit new loaded list
								};
								kSTRtrucktype = typeOf kOBJneartruck;											// Check type of nearest truck
								if ( kSTRtrucktype == "O_Truck_02_transport_F" ) then {							// If the nearest truck is KAMAZ:
									kARRnewload = kARRload + [_x];
									_x attachTo [kOBJneartruck,[0.07,-0.4,0.1]];								// load the 1st cargo into slot 1
									kOBJneartruck setvariable ["kARRholdcargo", kARRnewload, true];				// Set loaded cargo to 2
									kARRselected = kARRselected - [_x];											// remove the cargo from the active selection
									kARRloadedcargo = kARRloadedcargo + [_x];									// add cargo to loaded list
									publicVariable "kARRloadedcargo";											// Transmit new loaded list
								};
							If (_x == cargo1 ) then { kINTcargo1menuactive = 0;	};								// reset menu switch
							If (_x == cargo2 ) then { kINTcargo2menuactive = 0;	};								// reset menu switch
							};
						} forEach kARRselected;																	// end of foreach block
						kINTloadmenuactive = 0;																	// Set menu load truck inactive
						player removeAction kHANDloadaction;													// remove addaction menu
					}];
				};
			};
		};
	};
};