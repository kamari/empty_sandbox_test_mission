// -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-
// Cargo selection script for "Transport & Ambush"
// Written by Kamaradski 2014
// -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-

while {kINTcargoSystemActive == 1} do {
	{
		if kdebug then {hint format ["Running: \n %1", _x];};
		kOBJactiveLoop = _x;																		// add current looped object in variable
		KINTcargoState = _x in kARRselected;														// true if this object is found in the active selection array
		KINTcargoState2 = _x in kARRcargoAddactionActive;											// true if this object is found in the active addaction array

		if (!KINTcargoState && !KINTcargoState2) then {												// if this object is not selected && has no active addaction:
			kARRcargoAddactionActive = kARRcargoAddactionActive + [kOBJactiveLoop];					// indicate there is a active addaction on this object
			KAMASEL = kOBJactiveLoop addAction ["Select for loading...", {							// activate addaction
				_kThisOBJ = _this select 0;															// Save this object in a variable
				kARRselected = kARRselected + [_kThisOBJ];											// add cargo to selection
				kARRcargoAddactionActive = kARRcargoAddactionActive - [_kThisOBJ];					// indicate there is no active add-action on this object
				hint format ["current selection: \n %1", str kARRselected];							// message user
				removeAllActions _kThisOBJ;															// remove addaction menu
			}];
		};
		if (KINTcargoState && !KINTcargoState2) then {
			kARRcargoAddactionActive = kARRcargoAddactionActive + [kOBJactiveLoop];
			KAMASEL2 = kOBJactiveLoop addAction ["De-select cargo", {								// activate addaction
				_kThisOBJ = _this select 0;															// Save this object in a variable
				kARRselected = kARRselected - [_kThisOBJ];											// remove cargo from selection
				kARRcargoAddactionActive = kARRcargoAddactionActive - [_kThisOBJ];					// indicate there is no active add-action on this object
				hint format ["current selection: \n %1", str kARRselected];							// message user
				removeAllActions _kThisOBJ;															// remove addaction menu
			}];
		};
	sleep 1;
	} forEach kARRcargoLoop;
};
