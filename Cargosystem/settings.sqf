// -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-
// Cargosystem settings file for "Transport & Ambush"
// Written by Kamaradski 2014
// -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-

kDebug = false;
kARRvehicles = [truck1,truck2,truck3,truck4];						// Vehicles enabled for carrying loads
kARRcargoLoop = [cargo1,cargo2];									// Cargo enabled for loading
kINTdetachlevel = 0.02;												// at what damage level the cargo should detach from the truck
kINTcargoSystemActive = 1;											// switch for activating cargo system