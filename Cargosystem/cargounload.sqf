// -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-
// Cargo UNloading script for "Transport & Ambush"
// Written by Kamaradski 2014
// -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-

kFUNC_unlockcargo = {																							// Function to lock all cargo positions on truck 
	kOBJneartruck lockCargo [1, false];																			// re-enable cargo positions 
	kOBJneartruck lockCargo [2, false];
	kOBJneartruck lockCargo [3, false];
	kOBJneartruck lockCargo [4, false];
	kOBJneartruck lockCargo [5, false];
	kOBJneartruck lockCargo [6, false];
	kOBJneartruck lockCargo [7, false];
	kOBJneartruck lockCargo [8, false];
	kOBJneartruck lockCargo [9, false];
	kOBJneartruck lockCargo [10, false];
	kOBJneartruck lockCargo [11, false];
	kOBJneartruck lockCargo [12, false];
	kOBJneartruck lockCargo [13, false];
	kOBJneartruck lockCargo [14, false];
	kOBJneartruck lockCargo [15, false];
	kOBJneartruck lockCargo [16, false];
};																												// EOF


kFUNC_unload = {																								// Set function to handle unloading of the cargo
{
	if ( kINTunloadmenuactive <1) then {																		// Check if menu is already active
		_kARRnearobj = nearestObjects [player, ["Truck_01_base_F","Truck_02_base_F"], 10];						// list nearest trucks in array
		if ( count _kARRnearobj > 0 ) then {																	// if _kARRnearobj is not empty:
			kOBJneartruck = _kARRnearobj select 0;																// Select the first nearest object
			if ( kOBJneartruck in kARRvehicles) then {															// if the nearest truck is in the array:
			kARRunload = kOBJneartruck getVariable "kARRholdcargo";	
				if ( count kARRunload > 0 ) then {																// if something is loaded in the truck:
				kINTunloadmenuactive = 1;																		// set unload menu to active
					kHANDunload = kOBJneartruck addAction ["Unstrap cargo...", {								// activate addaction menu
						{
							detach _x;																			// Detach cargo
							kARRloadedcargo = kARRloadedcargo - [_x];											// remove cargo from the loaded list
							publicVariable "kARRloadedcargo";													// Transmit new loaded list
							kARRcargoAddactionActive = kARRcargoAddactionActive - [_x];
						} forEach kARRunload;																	// foreach cargo loaded in this truck
					kOBJneartruck setvariable ["kARRholdcargo", [], true];										// remove detached cargo from the cargo list
					kINTunloadmenuactive = 0;																	// Select unload menu to be in-active
					call kFUNC_unlockcargo;																		// unlock cargo positions
					kOBJneartruck removeAction kHANDunload;														// remove addaction
					}];
				};
			};	
		};
	};
};

while {kINTcargoSystemActive == 1} do {
	call kFUNC_unload;
	sleep 2;
}

