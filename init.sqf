if (!isServer && isNull player) then {isJIP=1;} else {isJIP=0;};
if (!isDedicated) then {waitUntil {!isNull player && isPlayer player};};

enableSaving [false, false];
enableSentences false;

// Random weather
execVM "scripts\randomWeather.sqf";

// Limited ammo system
execVM "scripts\ammo_box.sqf";
execVM "scripts\missionlogic.sqf";

// Cargo system
execVM "Cargosystem\cargosystem.sqf";

[] spawn {call compile preprocessFileLineNumbers "EPD\Ied_Init.sqf";};

// Time of day
if(isJIP == 0) then {
	curTimeHour = (paramsArray select 1);
	if (curTimeHour == 24) then {
		curTimeHour = floor(random 24)
	};
	setDate [1979,3,19, curTimeHour, 00];
	} else {
		waituntil {!isnull player};
	};